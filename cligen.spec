%global commit a4c706214191f1231adde21a84fe03c045bd02d5
%global gittag R3.5.0
%global shortcommit %(c=%{commit}; echo ${c:0:7})

Prefix:		/usr
Name:		cligen
Version:	3.5.0
Release:	1%{?dist}
Summary:	cligen

License:	GPL
URL:		http://www.cligen.se
Source0:  https://github.com/olofhagsand/%{name}/archive/%{commit}/%{name}-%{shortcommit}.tar.gz

BuildRequires:	flex
BuildRequires:	bison	
BuildRequires:	pkgconfig
#Requires:	

%description
cligen

%prep
%autosetup -n %{name}-%{commit}

%build
%configure
make


%install
make install DESTDIR=%{buildroot}


%files
%{_includedir}/cligen
%{_libdir}/libcligen.so*

%doc

%post
PKG_CONFIG_PATH=`pkg-config --variable pc_path pkg-config | cut -d":" -f1,1`
cat << EOF > $PKG_CONFIG_PATH/cligen.pc
prefix=$RPM_INSTALL_PREFIX
exec_prefix=\${prefix}
includedir=\${exec_prefix}/include
libdir=\${exec_prefix}/lib64

Name: cligen
Description: cligen
Version: 3.5.0
Cflags: -I\${includedir}
Libs: -L\${libdir} -lcligen
EOF
echo $RPM_INSTALL_PREFIX/lib64 > /etc/ld.so.conf.d/cligen.conf
/sbin/ldconfig

%postun
PKG_CONFIG_PATH=`pkg-config --silence-errors --variable pcfiledir cligen`
rm -f $PKG_CONFIG_PATH/cligen.pc
rm -f /etc/ld.so.conf.d/cligen.conf

%changelog

